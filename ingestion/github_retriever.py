import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

import pandas as pd
import time
import math


class GitHubRetriever(object):

    def __init__(self, pat):
        """
        :param pat: GitHub Personal Access Token
        """
        self.pat = pat
        retry_strategy = Retry(
            total=5,
            status_forcelist=[500, 502, 503, 504],
            backoff_factor=2
        )

        retry_adapter = HTTPAdapter(max_retries=retry_strategy)

        self.http = requests.Session()
        self.http.mount("https://", retry_adapter)
        self.http.mount("http://", retry_adapter)

    def _parse_github_projects(self, response, result: dict):

        for item in response.json()['items']:
            result['name'].append(item['name'])
            result['description'].append(item['description'])
            result['full_name'].append(item['full_name'])
            result['forks'].append(item['forks_count'])
            result['star'].append(item["stargazers_count"])
            result['created_at'].append(item['created_at'])
            result['updated_at'].append(item['updated_at'])
            result['open_issues'].append(item['open_issues_count'])

    def _get(self, url, param=None, header=None):
        """Return response object and next link if any"""
        result = None
        next_url = None
        if url is not None:
            response = self.http.get(url, params=param, headers=header)
            if response.status_code == 200:
                result = response
                if 'next' in response.links:
                    next_url = response.links['next']['url']
            else:
                print(response.status_code)
                print(response.json())
        return result, next_url

    def _get_wait_seconds(self, response):

        print("Response Links: ", response.links)

        rate_limit_min = int(response.headers['X-RateLimit-Limit'])
        wait_seconds = math.ceil(60.0 / rate_limit_min) + 1
        print(wait_seconds)
        return wait_seconds

    def get_github_projects(self, query, file_name: str):
        """
        Returns 100 GitHub projects that match the given query.
        :param query: Search query e.g: data_science+language:python
        :param file_name: Search results will be written to given path/file_name in csv format.
        :return: Dataframe of search results. Columns will be:
            name, description, matched, star, created_at, updated_at, and open_issues
        """
        url = 'https://api.github.com/search/repositories?per_page=100&page=1'
        params = {'q': query}
        headers = {'Accept': 'application/vnd.github.v3.text-match+json',
                   'Authorization': 'token {}'.format(self.pat)}

        # Setup result object
        result = {'name': [],
                  'description': [],
                  'full_name': [],
                  'forks': [],
                  'star': [],
                  'created_at': [],
                  'updated_at': [],
                  'open_issues': []
                  }

        response, next_url = self._get(url, params, headers)
        self._parse_github_projects(response, result)

        # Return dataframe
        result_df = pd.DataFrame(result)
        result_df = result_df.drop_duplicates()
        result_df = result_df.sort_values(by="star", ascending=False, ignore_index=True)

        result_df.to_csv("{}.csv".format(file_name))

        return result_df


    def get_project_issues(self, owner, project, file_to_write):
        """
        API Doc: https://docs.github.com/en/rest/reference/issues#list-repository-issues
            - We get all issues for all states {open, closed, or all}, these include pull requests
        :param owner: Owner of the project
        :param project: Name of the project
        :param file_to_write: Where to write the results dataframe
        :return: Dataframe
        """
        url = 'https://api.github.com/repos/{}/{}/issues?filter=all;state=all;per_page=100'.format(owner, project)
        headers = {'Accept': 'application/vnd.github.v3+json', 'Authorization': 'token {}'.format(self.pat)}

        # Get rate limit and pages information
        response = self.http.get(url, params=None, headers=headers)
        wait_seconds = self._get_wait_seconds(response)

        # Setup result object
        result = {'issue_id': [],
                  'title': [],
                  'user_id': [],
                  'state': [],
                  'created_at': [],
                  'closed_at': [],
                  'labels': [],
                  'text': []}

        response, next_url = self._get(url, param=None, header=headers)
        self._parse_issues(response=response, result=result)

        while next_url is not None:
            time.sleep(wait_seconds)
            print("Getting next page: " + next_url)
            response, next_url = self._get(next_url, param=None, header=headers)
            if response is not None:
                self._parse_issues(response, result)

        # Return dataframe
        result_df = pd.DataFrame(result)
        result_df = result_df.drop_duplicates()

        result_df.to_csv("{}.csv".format(file_to_write))

        return result_df

    def _parse_pulls(self, response, result: dict):
        for item in response.json():
            result['issue_id'].append(item["number"])
            result['state'].append(item["state"])
            result['user_id'].append(item["user"]["id"])
            result['created_at'].append(item["created_at"])
            result['closed_at'].append(item['closed_at'])
            result['merged_at'].append(item['merged_at'])

    def get_pull_requests(self, owner, project, state, file_to_write):
        """

        :param owner:
        :param project:
        :param state: closed, all, or open
        :param file_to_write:
        :return:
        """
        url = "https://api.github.com/repos/tensorflow/tensorflow/pulls?state={}".format(state)
        headers = {'Accept': 'application/vnd.github.v3+json', 'Authorization': 'token {}'.format(self.pat)}

        # Get rate limit and pages information
        response = self.http.get(url, params=None, headers=headers)
        wait_seconds = self._get_wait_seconds(response)

        # Setup result object
        result = {'issue_id': [],
                  'state': [],
                  'user_id': [],
                  'created_at': [],
                  'closed_at': [],
                  'merged_at': []}

        response, next_url = self._get(url, param=None, header=headers)
        self._parse_pulls(response=response, result=result)

        while next_url is not None:
            time.sleep(wait_seconds)
            print("Getting next page: " + next_url)
            response, next_url = self._get(next_url, param=None, header=headers)
            if response is not None:
                self._parse_pulls(response, result)

        # Return dataframe
        result_df = pd.DataFrame(result)
        result_df = result_df.drop_duplicates()

        result_df.to_csv("{}.csv".format(file_to_write))

        return result_df
