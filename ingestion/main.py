from ingestion.github_retriever import GitHubRetriever
import time
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Retrieves and parses project issues and meta from Github')
    parser.add_argument('--pat', help='GitHub personal access token', required=True)
    args = parser.parse_args()

    github = GitHubRetriever(args.pat)

    # Get Project Status
    # GitHub API doesn't have endpoint to get a project by owner and project.
    pytorch_projects = github.get_github_projects(query='pytorch/pytorch', file_name="pytorch_projects")
    tensorflow_projects = github.get_github_projects(query='tensorflow/tensorflow', file_name="tensorflow_projects")

    # Get Tensorflow issues
    issues = github.get_project_issues(owner="tensorflow", project="tensorflow", file_to_write="tensorflow_issues")
    print("Got Tensorflow {} issues".format(len(issues)))
    print(issues.head(20))

    time.sleep(120)

    # get Pytorch issues
    issues = github.get_project_issues(owner="pytorch", project="pytorch", file_to_write="pytorch_issues")
    print("Got PyTorch {} issues".format(len(issues)))
    print(issues.head(20))
