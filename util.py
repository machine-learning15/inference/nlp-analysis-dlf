import pandas as pd
from collections import Counter
import textacy.preprocessing as tprep
import numpy as np
import re
import textacy
import ast
from gensim.models import Word2Vec, FastText, KeyedVectors
import util
from umap import UMAP
import csv


def read_file(path: str):
    df = pd.read_csv(path, index_col=0)
    df["created_at"] = pd.to_datetime(df["created_at"])
    df = df[["issue_id", "user_id", "created_at", "closed_at", "title", "state", "labels", "text"]]

    return df


def read_projects():
    pt = pd.read_csv("data/pytorch_projects.csv", index_col=0)
    tf = pd.read_csv("data/tensorflow_projects.csv", index_col=0)
    pt = pt[pt["full_name"] == "pytorch/pytorch"]
    tf = tf[tf["full_name"] == "tensorflow/tensorflow"]
    pop = pd.concat([pt, tf])
    return pop


def read_issues():
    """Read tensorflow and pytorch issues"""
    tf = read_file("data/tensorflow_issues.csv")
    tf["project"] = "tensorflow"

    # pytorch
    pt = read_file("data/pytorch_issues.csv")
    pt["project"] = "pytorch"

    # concat all to the same dataframe
    issues = pd.concat([tf, pt])

    return issues


def prep(text):
    """Clean up text:
    - Replace URLs with url
    - Normalize all quotation marks
    - Normalize white space
    - Remove []
    - Remove all punctuation except . at the end of titles
    - Convert to lowercase
    """

    text = tprep.replace.urls(text, "url")
    text = tprep.normalize.quotation_marks(text)
    text = tprep.normalize.whitespace(text)
    text = tprep.normalize.repeating_chars(text=text, chars=".*-")

    text = text.replace("[", "").replace("]", "")
    # keep .
    text = text.replace(r'[!@#$%^&*()+,;:~-=_]', '')
    text = re.sub(r'[\[\]{}"`\']', '', text)
    text = re.sub(r"('t\s|'s\s|\st\s|\ss\s)", ' ', text)
    text = re.sub(r'[\s]{2,}', '', text)
    # remove . if at the end of the text
    if text[-1] == ".":
        text = text[:-1]

    return text.lower()


def extract_lemmas(doc, **kwargs):
    doc = textacy.make_spacy_doc(doc.text, lang="en_core_web_sm")
    tokens = textacy.extract.basics.words(doc, **kwargs)
    result = [token.lemma_ for token in tokens]
    return result


def extract_nlp(doc):
    """
    - Preprocess text with textacy
    - Tokenize and lemmatize with spacy
    - Get Adjectives, nouns, verbs and proper nouns
    - Get phrases
    - Get bigrams
    """

    result = {
        'lemmas': extract_lemmas(doc, filter_punct=False, include_pos=['ADJ', 'NOUN', 'VERB', 'PROPN']),
        'bigrams': list(textacy.extract.ngrams(doc, 2)),
        'noun_noun': list(textacy.extract.matches.token_matches(doc, patterns=["POS:NOUN POS:NOUN:+"])),
        'adj_noun': list(textacy.extract.matches.token_matches(doc, patterns=["POS:ADJ POS:NOUN:+"])),
        'verb_noun': list(textacy.extract.matches.token_matches(doc, patterns=["POS:VERB POS:NOUN:+"]))
    }

    return result


def process_spans(df):
    df["bigrams"] = df["bigrams"].map(lambda x: [t.text for t in x])
    df["noun_noun"] = df["noun_noun"].map(lambda x: [t.text for t in x])
    df["adj_noun"] = df["adj_noun"].map(lambda x: [t.text for t in x])
    df["verb_noun"] = df["verb_noun"].map(lambda x: [t.text for t in x])


def compute_df_idf(df, column, min_freq=2):
    counter = Counter()

    def update(tokens):
        counter.update(set(tokens))

    df[column].map(update)

    # transform counter into a DataFrame
    idf_df = pd.DataFrame.from_dict(counter, orient='index', columns=['df'])
    idf_df = idf_df.query('df >= @min_freq')
    idf_df['idf'] = np.log(len(df) / idf_df['df']) + 0.1
    idf_df.index.name = column

    return idf_df

def convert_tolist(df, columns):
    """Convert string list to list. E.g '['a','b']' -> ['a','b']"""
    for col in columns:
        df[col] = df[col].map(lambda x: ast.literal_eval(x))
        
def save_models(df, column, param_grid, name="tf", vector_size=100):
    """
    Create variety of embeddings, based on the input hyperparameter grid.
    Save the models to disk, returns file names.
    """
    sents = df[column].to_list()
    file_names=[]
    for algo, params in param_grid.items():
        for variant in params['variant']:
            sg = 1 if variant == 'sg' else 0
            for window in params['window']:
                for min_count in params["min_count"]:
                    if algo == 'w2v':
                        model = Word2Vec(sents, vector_size=vector_size, window=window, sg=sg, min_count=min_count)
                    else:
                        model = FastText(sents, vector_size=vector_size, window=window, sg=sg, min_count=min_count)

                    file_name = f"{name}_{column}_{algo}_{variant}_{window}_{min_count}.bin"
                    file_names.append(file_name)
                    model.wv.save_word2vec_format(file_name, binary=True)
                    
    return file_names

def compare_models(models, **kwargs):
    """For each model, if phrase exists in vocabulary, then get most similar phrases"""
    phrase=kwargs.get('positive', 'NOT_AVAILABLE')
    vocabulary = set()
    df = pd.DataFrame()
    for name, model in models:
        if phrase in model.key_to_index:
            vocabulary.update(list(model.key_to_index.keys()))
            df[name] = [f"{word} {score:.3f}" for word, score in model.most_similar(**kwargs)]   
    return vocabulary, df

    
def write_model(models, model_name):
    """Write embedding model vectors and meta to file compatible with tensorflow embedding projection. """
    model = models[model_name]    
    
    with open(f'projection/{model_name}_vecs.tsv', 'w', encoding='utf-8') as tsvfile:
        writer = csv.writer(tsvfile, delimiter='\t', dialect=csv.unix_dialect, quoting=csv.QUOTE_MINIMAL)    
        for i, word in enumerate(model.index_to_key):
            _ = writer.writerow(model[word].tolist())
        
    
    with open(f'projection/{model_name}_words.tsv', 'w', encoding='utf-8') as tsvfile:
        tsvfile.write('\n'.join(list(model.key_to_index.keys())))
