# Comparison of Pytorch and Tensorflow GitHub Projects
The focus of this project is to gain insight into these projects' health by looking at the reported [issues](https://docs.github.com/en/issues/tracking-your-work-with-issues/about-issues) on GitHub.

## Dataset
We retrieved project and issue data using [GitHub Rest API](https://docs.github.com/en/rest).  Ingestion code can be adopted to analyze other open source projects on Github. To identify most common issues, we extracted most common noun phrases from issues which were open at the time of ingestion. 
Data is retrieved on 2022-02-24. 

**Columns**:
- issue_created_at
- issue title

## Findings

[Tensorflow](https://github.com/tensorflow/tensorflow) project started in 2016 August, and [Pytorch](https://github.com/pytorch/pytorch) project started 9 months later in August 2016.  Number of issues for Pytorch project surpassed Tensorflow in 2018. This might be due to popularity of the project; perhaps as more people use it, more issues get created.
Currently, there are 11,342 open issues on PyTorch project, and 2549 open issues on Tensorflow.

<img src="./img/meta.png" alt="meta" width="750">

Users create issues when they encounter a bug or make a feature request.  As a project is adopted more, the number of issues opened by users might increase.  Below graph shows the top 20 phrases in open issue titles.  Both projects have high number of feature requests and memory leak issues.  The quality and insightfulness of below issues ultimately depends on how detailed and precise these issue descriptions are.  It is possible that when a user creates an issue, they might not identify it correctly.

<img src="./img/top.png" alt="top" width=500>

We can further explore the terms related to memory issues for both projects using word embeddings. Below graph shows top 10 similar phrases to "memory leak" for both projects. 

<img src="./img/memory_issues.jpg" alt="top" width=500>

We see different type of problems related to memory issues for each project.  Both projects report memory issues related to mobile versions, as mobile devices are more sensitive to memory load.  Gpu and cuda related problems are prominent in Pytorch project. 

## Next Steps
Further exploration is possible using word embeddings with [Tensorflow Embedding Projection](https://projector.tensorflow.org).  Required vector and meta files are in the projection folder.   


